var websock;
var tabindex = $("#topics > div").length * 10 + 100;
var maxTopics = 3;

function doUpdate() {
    var data = $("#formSave").serializeArray();
    websock.send(JSON.stringify({'config': data}));
    return false;
}

function doReset() {
    var response = window.confirm("Are you sure you want to reset the device?");
    if (response == false) return false;
    websock.send(JSON.stringify({'action': 'reset'}));
    return false;
}

function doReconnect() {
    var response = window.confirm("Are you sure you want to disconnect from the current WIFI network?");
    if (response == false) return false;
    websock.send(JSON.stringify({'action': 'reconnect'}));
    return false;
}

function showPanel() {
    $(".panel").hide();
    $("#" + $(this).attr("data")).show();
    if ($("#layout").hasClass('active')) toggleMenu();
};

function addTopic() {

    var numTopics = $("#topics > div").length;
    if (numTopics >= maxTopics) {
        alert("Max number of topics reached");
        return;
    }

    var template = $("#template-topics .pure-g")[0];
    var line = $(template).clone();
    $(line).find("input").each(function() {
        $(this).attr("tabindex", tabindex++);
    });
    $(line).find(".button-del-topic").on('click', delTopic);
    $(line).find(".button-more-topic").on('click', moreTopic);
    line.appendTo("#topics");

    $(":checkbox", line).iphoneStyle({
        resizeContainer: true,
        resizeHandle: true,
        checkedLabel: 'YES',
        uncheckedLabel: 'NO'
    });

}

function delTopic() {
    var parent = $(this).parents(".pure-g");
    $(parent).remove();
}

function moreTopic() {
    var parent = $(this).parents(".pure-g");
    $("div.more", parent).toggle();
}

function toggleMenu() {
    $("#layout").toggleClass('active');
    $("#menu").toggleClass('active');
    $("#menuLink").toggleClass('active');
}

function processData(data) {

    // pre-process
    if ("network" in data) {
        data.network = data.network.toUpperCase();
    }
    if ("mqttStatus" in data) {
        data.mqttStatus = data.mqttStatus ? "CONNECTED" : "NOT CONNECTED";
    }

    // title
    if ("app" in data) {
        $(".pure-menu-heading").html(data.app);
        var title = data.app;
        if ("hostname" in data) {
            title = data.hostname + " - " + title;
        }
        document.title = title;
    }

    // automatic assign
    Object.keys(data).forEach(function(key) {

        // Look for INPUTs
        var element = $("input[name=" + key + "]");
        if (element.length > 0) {
            if (element.attr('type') == 'checkbox') {
                element.prop("checked", data[key] == 1)
                    .iphoneStyle({
                        resizeContainer: true,
                        resizeHandle: true,
                        checkedLabel: 'ON',
                        uncheckedLabel: 'OFF'
                    })
                    .iphoneStyle("refresh");
            } else {
                element.val(data[key]);
            }
        }

        // Look for SELECTs
        var element = $("select[name=" + key + "]");
        if (element.length > 0) {
            element.val(data[key]);
        }

    });

    // WIFI
    var groups = $("#panel-wifi .pure-g");
    for (var i in data.wifi) {
        var wifi = data.wifi[i];
        Object.keys(wifi).forEach(function(key) {
            var id = "input[name=" + key + "]";
            if ($(id, groups[i]).length) $(id, groups[i]).val(wifi[key]);
        });
    };

    // TOPICS
    if ("topics" in data) {

        maxTopics = parseInt(data.maxTopics);
        for (var i in data.topics) {

            // add a new row
            addTopic();

            // get group
            var line = $("#topics .pure-g")[i];

            // fill in the blanks
            var topic = data.topics[i];
            Object.keys(topic).forEach(function(key) {
                var element = $("input[name=" + key + "]", line);
                if (element.length) {
                    if (element.attr('type') == 'checkbox') {
                        element.prop("checked", topic[key] == 1)
                            .iphoneStyle("refresh");
                    } else {
                        element.val(topic[key]);
                    }
                }
            });

        }
    }

}

function getJson(str) {
    try {
        return JSON.parse(str);
    } catch (e) {
        return false;
    }
}

function init() {

    $("#menuLink").on('click', toggleMenu);
    $(".button-update").on('click', doUpdate);
    $(".button-reset").on('click', doReset);
    $(".button-reconnect").on('click', doReconnect);
    $(".button-add-topic").on('click', addTopic);
    $(".pure-menu-link").on('click', showPanel);

    var host = window.location.hostname;
    websock = new WebSocket('ws://' + host + ':81/');
    websock.onopen = function(evt) {};
    websock.onclose = function(evt) {};
    websock.onerror = function(evt) {};
    websock.onmessage = function(evt) {
        var data = getJson(evt.data);
        if (data) processData(data);
    };

}

$(init);
