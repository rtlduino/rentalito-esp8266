/*

RENTALITO
DISPLAY MODULE

Copyright (C) 2016 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include "ht1632c.h"

ht1632c matrix = ht1632c(MATRIX_DATA_PIN, MATRIX_WR_PIN, MATRIX_CLK_PIN, MATRIX_CS_PIN, 2);

// -----------------------------------------------------------------------------
// DISPLAY
// -----------------------------------------------------------------------------

void displayText(uint8_t x, uint8_t y, char * text, uint8_t color, uint8_t align = ALIGN_LEFT | ALIGN_TOP, uint8_t bufferID = 0) {
    byte width = strlen(text) * matrix.getFontWidth();
    if (width > matrix.getDisplayWidth()) {
        matrix.hScroll(y, text, color, SCROLL_LEFT, 20, bufferID);
    } else {
        matrix.putText(x, y, text, color, align);
    }
}

void displayShow(display_mode_t mode, char * row1, char * row2) {

    matrix.stopScrolling();
    matrix.clear();

    if (mode == DISPLAY_MESSAGE) {
        matrix.setFont(FONT_8x13B);
        displayText(0, 2, row1, ORANGE, ALIGN_CENTER);
    }

    if (mode == DISPLAY_MESSAGE_2LINES) {
        matrix.setFont(FONT_5x7);
        displayText(0, 1, row1, RED, ALIGN_CENTER, 0);
        displayText(0, 9, row2, RED, ALIGN_CENTER, 1);
    }

    if (mode == DISPLAY_KEY) {
        matrix.setFont(FONT_8x8);
        displayText(0, 1, row1, RED, ALIGN_CENTER, 0);
        matrix.setFont(FONT_5x7);
        displayText(0, 9, row2, GREEN, ALIGN_CENTER, 1);
    }

    if (mode == DISPLAY_VALUE) {
        matrix.setFont(FONT_5x7);
        displayText(0, 1, row1, RED, ALIGN_CENTER, 0);
        matrix.setFont(FONT_8x8);
        displayText(0, 8, row2, GREEN, ALIGN_CENTER, 1);
    }

    matrix.sendframe();

}

void displayShow(display_mode_t mode, char * row1) {
    displayShow(mode, row1, "");
}

void displayClear() {
    matrix.stopScrolling();
    matrix.clear();
    matrix.sendframe();
    //DEBUG_MSG("[DISPLAY] Clear display");
}

void displaySetBrightness() {
    uint8_t brightness = getSetting("brightness", String(MATRIX_BRIGHTNESS)).toInt();
    matrix.setBrightness(brightness);
    DEBUG_MSG("[DISPLAY] Brightness: %d\n",brightness);
}

void displaySetup() {
    matrix.begin();
    matrix.setBrightness(MATRIX_BRIGHTNESS);
    displaySetBrightness();
}

void displayLoop() {

    matrix.scroll();

    static unsigned long last_tick = 0;
    static bool tick = false;
    if (workingMode == 0) {
        if (millis() - last_tick > DISPLAY_TICK_TIME) {
            last_tick = millis();
            tick = !tick;
            matrix.setPixel(63, 15, tick ? GREEN : BLACK);
            matrix.sendframe();
        }
    }

}

bool displayScrolling() {
    return matrix.scrolling();
}
