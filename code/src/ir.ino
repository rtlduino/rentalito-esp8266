/*

RENTALITO
IR MODULE

Copyright (C) 2016 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include <IRremoteESP8266.h>

IRrecv irReceiver(IR_PIN);
decode_results results;

// -----------------------------------------------------------------------------
// IR
// -----------------------------------------------------------------------------

void irSetup() {
    irReceiver.enableIRIn();
}

void irLoop() {
    if (irReceiver.decode(&results)) {

        DEBUG_MSG("[IR] Code: %08X\n", results.value);

        // Bright up
        if (results.value == 0xBB750745) {
            uint8_t brightness = getSetting("displayBrightness", String(MATRIX_BRIGHTNESS)).toInt();
            if (brightness < 15) {
                setSetting("displayBrightness", String() + ++brightness);
                displaySetBrightness();
            }
        }

        // Bright down
        if (results.value == 0x29EB360B) {
            uint8_t brightness = getSetting("displayBrightness", String(MATRIX_BRIGHTNESS)).toInt();
            if (brightness > 0) {
                setSetting("displayBrightness", String() + --brightness);
                displaySetBrightness();
            }
        }

        // 0xBB750745 => bright up
        // 0x29EB360B => bright down
        // 0x4C4AB981 => OFF
        // 0x3FF7E521 => ON

        irReceiver.resume();

    }

    delay(100);

}
