/*

RENTALITO
LOG MODULE

Copyright (C) 2016 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include <stdio.h>
#include <stdarg.h>

/*
#ifdef DEBUG_PORT
    #define DEBUG_MSG(...) DEBUG_PORT.printf( __VA_ARGS__ )
#else
    #define DEBUG_MSG(...)
#endif
*/

// -----------------------------------------------------------------------------
// LOG
// -----------------------------------------------------------------------------

void log(const char * format, ...) {
    va_list args;
    va_start (args, format);
    DEBUG_MSG(format, args);
    va_end (args);
}
