/*

RENTALITO
WEBSERVER MODULE

Copyright (C) 2016 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include <WebSocketsServer.h>
#include <Hash.h>
#include <ArduinoJson.h>

WebSocketsServer webSocket = WebSocketsServer(81);

// -----------------------------------------------------------------------------
// WEBSOCKETS
// -----------------------------------------------------------------------------

bool webSocketSend(char * payload) {
    //DEBUG_MSG("[WEBSOCKET] Broadcasting '%s'\n", payload);
    webSocket.broadcastTXT(payload);
}

bool webSocketSend(uint8_t num, char * payload) {
    //DEBUG_MSG("[WEBSOCKET] Sending '%s' to #%d\n", payload, num);
    webSocket.sendTXT(num, payload);
}

void webSocketParse(uint8_t num, uint8_t * payload, size_t length) {

    // Parse JSON input
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject((char *) payload);
    if (!root.success()) {
        DEBUG_MSG("[WEBSOCKET] Error parsing data\n");
        return;
    }

    // Check actions
    if (root.containsKey("action")) {

        String action = root["action"];
        DEBUG_MSG("[WEBSOCKET] Requested action: %s\n", action.c_str());

        if (action.equals("reset")) ESP.reset();
        if (action.equals("reconnect")) wifiDisconnect();


    };

    // Check config
    if (root.containsKey("config") && root["config"].is<JsonArray&>()) {

        JsonArray& config = root["config"];
        DEBUG_MSG("[WEBSOCKET] Parsing configuration data\n");

        bool dirty = false;
        bool dirtyMQTT = false;
        bool dirtyTopics = false;
        unsigned int network = 0;
        unsigned int topicCount = getSetting("topicCount", "0").toInt();
        unsigned int topics = 0;
        workingMode = 0;
        bool prioritarySet = false;
        bool isTopic = false;

        for (unsigned int i=0; i<config.size(); i++) {

            yield();

            String key = config[i]["name"];
            String value = config[i]["value"];
            isTopic = false;

            if (key == "workingMode") {
                workingMode = 1;
                continue;
            }

            if (key == "ssid") {
                key = key + String(network);
            }
            if (key == "pass") {
                key = key + String(network);
                ++network;
            }

            if (key == "topic") {
                if (value == "") break;
                key = key + String(topics);
                isTopic = true;

                // Checkboxes do not send key if unchecked (WTF!!!)
                // so we manage "prioritary" option here
                if (topics>0) {
                    if (!prioritarySet) {
                        String pkey = String("prioritary") + String(topics-1);
                        if (getSetting(pkey).toInt() == 1) {
                            setSetting(pkey, String() + "0");
                            dirty = true;
                            dirtyTopics = true;
                        }
                    }
                }
                prioritarySet = false;

            }
            if (key == "name") {
                key = key + String(topics);
                isTopic = true;
            }
            if (key == "value") {
                key = key + String(topics);
                isTopic = true;
            }
            if (key == "filter") {
                key = key + String(topics);
                isTopic = true;
            }
            if (key == "prioritary") {
                key = key + String(topics);
                value = String(1);
                isTopic = true;
                prioritarySet = true;
            }
            if (key == "expire") {
                key = key + String(topics);
                isTopic = true;
                ++topics;
            }

            if (value != getSetting(key)) {
                setSetting(key, value);
                dirty = true;
                if (key.startsWith("mqtt")) dirtyMQTT = true;
                if (isTopic) dirtyTopics = true;
            }

        }

        // Last prioritary
        if (!prioritarySet) {
            String pkey = String("prioritary") + String(topics-1);
            if (getSetting(pkey).toInt() == 1) {
                setSetting(pkey, String() + "0");
                dirty = true;
                dirtyTopics = true;
            }
        }

        // New or deleted topics
        if (topicCount != topics) {

            // delete remaining mapping
            for (unsigned int i=topics; i<topicCount; i++) {
                delSetting("nodeid" + String(i));
                delSetting("name" + String(i));
                delSetting("value" + String(i));
                delSetting("filter" + String(i));
                delSetting("prioritary" + String(i));
                delSetting("expire" + String(i));
            }

            setSetting("topicCount", String() + topics);
            dirty = true;
            dirtyTopics = true;

        }

        // Set mode
        if (workingMode != getSetting("workingMode").toInt()) {
            setSetting("workingMode", String() + workingMode);
            dirty = true;
        }

        // Save settings
        if (dirty) {
            saveSettings();
            displaySetBrightness();
            wifiConfigure();
        }

        // Check if we should remove channel data
        if (dirtyTopics) {
            clearChannels();
        }

        // Check if we should reconfigure MQTT connection
        if (dirtyMQTT) {
            mqttDisconnect();
        }

        // Show feedback
        if (dirty) {
            displayShow(DISPLAY_MESSAGE_2LINES, "CONFIG", "SAVED");
        } else {
            DEBUG_MSG("[WEBSOCKET] No changes in configuration\n");
            displayShow(DISPLAY_MESSAGE_2LINES, "NO", "CHANGES");
        }
        display_last_update = millis();

    }

}

void webSocketStart(uint8_t num) {

    char app[64];
    sprintf(app, "%s %s", APP_NAME, APP_VERSION);

    char chipid[6];
    sprintf(chipid, "%06X", ESP.getChipId());

    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();

    root["app"] = app;
    root["hostname"] = getSetting("hostname", HOSTNAME);
    root["chipid"] = chipid;
    root["mac"] = WiFi.macAddress();
    root["network"] = getNetwork();
    root["ip"] = getIP();
    root["workingMode"] = workingMode;
    root["mqttStatus"] = mqttConnected() ? "1" : "0";
    root["mqttServer"] = getSetting("mqttServer", MQTT_SERVER);
    root["mqttPort"] = getSetting("mqttPort", String(MQTT_PORT));
    root["mqttUser"] = getSetting("mqttUser");
    root["mqttPassword"] = getSetting("mqttPassword");
    root["brightness"] = getSetting("brightness", String(MATRIX_BRIGHTNESS)).toInt();
    root["temperature"] = getTemperature();
    root["humidity"] = getHumidity();

    JsonArray& wifi = root.createNestedArray("wifi");
    for (byte i=0; i<3; i++) {
        JsonObject& network = wifi.createNestedObject();
        network["ssid"] = getSetting("ssid" + String(i));
        network["pass"] = getSetting("pass" + String(i));
    }

    byte topicCount = getSetting("topicCount", "0").toInt();
    root["maxTopics"] = MAX_TOPICS;
    root["topicCount"] = topicCount;
    JsonArray& topics = root.createNestedArray("topics");
    for (byte i=0; i<topicCount; i++) {
        JsonObject& topic = topics.createNestedObject();
        topic["topic"] = getSetting("topic" + String(i));
        topic["name"] = getSetting("name" + String(i));
        topic["value"] = getSetting("value" + String(i));
        topic["filter"] = getSetting("filter" + String(i));
        topic["prioritary"] = getSetting("prioritary" + String(i));
        topic["expire"] = getSetting("expire" + String(i));
    }

    String output;
    root.printTo(output);
    webSocket.sendTXT(num, (char *) output.c_str());

}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {

    switch(type) {
        case WStype_DISCONNECTED:
            DEBUG_MSG("[WEBSOCKET] #%u disconnected\n", num);
            break;
        case WStype_CONNECTED:
            #if DEBUG_PORT
                {
                    IPAddress ip = webSocket.remoteIP(num);
                    DEBUG_MSG("[WEBSOCKET] #%u connected, ip: %d.%d.%d.%d, url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
                }
            #endif
            webSocketStart(num);
            break;
        case WStype_TEXT:
            //DEBUG_MSG("[WEBSOCKET] #%u sent: %s\n", num, payload);
            webSocketParse(num, payload, length);
            break;
        case WStype_BIN:
            //DEBUG_MSG("[WEBSOCKET] #%u sent binary length: %u\n", num, length);
            webSocketParse(num, payload, length);
            break;
    }

}

void webSocketSetup() {
    webSocket.begin();
    webSocket.onEvent(webSocketEvent);
}

void webSocketLoop() {
    webSocket.loop();
}
