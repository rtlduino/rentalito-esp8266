//------------------------------------------------------------------------------
// SET BY PLATFORMIO
//------------------------------------------------------------------------------

//#define DEBUG_PORT              Serial

// -----------------------------------------------------------------------------
// HARDWARE
// -----------------------------------------------------------------------------

#define SERIAL_BAUDRATE         115200
#define LED_PIN                 0

// -----------------------------------------------------------------------------
// MATRIX
// -----------------------------------------------------------------------------

#define MATRIX_DATA_PIN         12
#define MATRIX_WR_PIN           13
#define MATRIX_CS_PIN           14
#define MATRIX_CLK_PIN          16
#define MATRIX_BRIGHTNESS       0x0F

// -----------------------------------------------------------------------------
// WIFI
// -----------------------------------------------------------------------------

#define WIFI_RECONNECT_INTERVAL 300000
#define WIFI_MAX_NETWORKS       3
#define AP_PASS                 "fibonacci"
#define OTA_PASS                "fibonacci"
#define OTA_PORT                8266

// -----------------------------------------------------------------------------
// MQTT
// -----------------------------------------------------------------------------

#define MQTT_SERVER             "192.168.1.100"
#define MQTT_PORT               1883
#define MQTT_RETAIN             true
#define MQTT_RECONNECT_DELAY    10000
#define MQTT_IP_TOPIC           "/device/rentalito/ip"
#define MQTT_VERSION_TOPIC      "/device/rentalito/version"
#define MQTT_FSVERSION_TOPIC    "/device/rentalito/fsversion"
#define MQTT_HEARTBEAT_TOPIC    "/device/rentalito/heartbeat"
#define MQTT_MODE_TOPIC         "/device/rentalito/mode"
#define MQTT_BRIGHTNESS_TOPIC   "/device/rentalito/brightness"

// -----------------------------------------------------------------------------
// NTP
// -----------------------------------------------------------------------------

#define NTP_SERVER              "pool.ntp.org"
#define NTP_TIME_OFFSET         1
#define NTP_DAY_LIGHT           true
#define NTP_UPDATE_INTERVAL     1800

// -----------------------------------------------------------------------------
// DHT
// -----------------------------------------------------------------------------

#define DHT_PIN                 5
#define DHT_UPDATE_INTERVAL     300000
#define DHT_TYPE                DHT22
#define DHT_TIMING              11
#define DHT_TEMPERATURE_TOPIC   "/home/studio/temperature"
#define DHT_HUMIDITY_TOPIC      "/home/studio/humidity"

// -----------------------------------------------------------------------------
// IR
// -----------------------------------------------------------------------------

#define IR_PIN                  4

// -----------------------------------------------------------------------------
// DEFAULTS
// -----------------------------------------------------------------------------

#define HEARTBEAT_INTERVAL      300000
#define HOSTNAME                APP_NAME
#define DEVICE                  APP_NAME
#define DEFAULT_WORKING_MODE    1
#define MAX_TOPICS              20
#define MAX_VALUE_LENGTH        20
#define DISPLAY_UPDATE_TIME     5000
#define DISPLAY_TICK_TIME       1000
