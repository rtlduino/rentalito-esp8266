/*

RENTALITO
MAIN MODULE

HT1632C-based MQTT Display

Copyright (C) 2016 by Xose Pérez <xose dot perez at gmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "version.h"
#include "defaults.h"
#include "debug.h"
#include <WebSockets.h>
#include "FS.h"
#include <NtpClientLib.h>

struct channel_t {
    char value[MAX_VALUE_LENGTH+1];
    unsigned long last;
};

typedef enum {
    DISPLAY_MESSAGE,
    DISPLAY_MESSAGE_2LINES,
    DISPLAY_KEY,
    DISPLAY_VALUE,
} display_mode_t;

channel_t channel[MAX_TOPICS];
unsigned char current_channel = 0;
unsigned long display_last_update = 0;

// mode 0 means only alerts, mode 1 all messages
uint8_t workingMode = 1;

String getSetting(const String& key, String defaultValue = "");

// -----------------------------------------------------------------------------
// Channels
// -----------------------------------------------------------------------------

void clearChannels() {
    for (byte i=0; i<MAX_TOPICS; i++) {
        channel[i].last = 0;
    }
}

void clearScreen() {
    displayClear();
    display_last_update = millis();
}

void showWaiting() {
    displayShow(DISPLAY_MESSAGE_2LINES, (char *) "WAITING...");
    display_last_update = millis();
}

void showWelcome() {
    displayShow(DISPLAY_MESSAGE_2LINES, (char *) APP_NAME, (char *) APP_VERSION);
    display_last_update = millis();
}

void showNextChannel() {
    unsigned char index = current_channel;
    while (true) {
        index = (index + 1) % MAX_TOPICS;
        if (showChannel(index)) break;
        if (index == current_channel) {
            showWaiting();
            break;
        }
    }
    current_channel = index;
}

bool showChannel(unsigned char index) {

    // No message arrived to this channel yet
    if (channel[index].last == 0) return false;

    // Check expiration
    unsigned long expire = 1000 * getSetting("expire" + String(index)).toInt();
    if ((millis() - channel[index].last) > expire) return false;

    // Check filter
    String filter = getSetting("filter" + String(index));
    if ((filter.length()>0) && (!filter.equals(channel[index].value))) return false;

    // Get the name and value templates and replace the actual value
    String name = getSetting("name" + String(index));
    name.replace("{value}", channel[index].value);
    String value = getSetting("value" + String(index));
    value.replace("{value}", channel[index].value);

    if (value.length() == 0) {
        //DEBUG_MSG("[DISPLAY] %s\n", (char *) name.c_str());
        displayShow(DISPLAY_MESSAGE, (char *) name.c_str());
    } else {
        //DEBUG_MSG("[DISPLAY] %s => %s\n", (char *) name.c_str(), (char *) value.c_str());
        displayShow(DISPLAY_VALUE, (char *) name.c_str(), (char *) value.c_str());
    }

    display_last_update = millis();

    return true;

}

void matchTopic(char* topic, byte* payload, unsigned int length) {

    // Find topic
    byte topics = getSetting("topicCount").toInt();
    for (byte i=0; i<topics; i++) {
        if (getSetting("topic" + String(i)).equals(topic)) {

            // Match! Blink in happyness
            ledOn();

            // Store values
            channel[i].last = millis();
            byte len = length > MAX_VALUE_LENGTH ? MAX_VALUE_LENGTH : length;
            memcpy(channel[i].value, payload, len);
            //DEBUG_MSG("[MQTT] Matched %s %s\n", topic, channel[i].value);

            // Check prioritary
            if (getSetting("prioritary" + String(i)).equals("1")) {
                showChannel(i);
            }

            delay(10);
            ledOff();

            break;

        }
    }

}


// -----------------------------------------------------------------------------
// Common methods
// -----------------------------------------------------------------------------

void ledOn() {
    digitalWrite(LED_PIN, LOW);
}

void ledOff() {
    digitalWrite(LED_PIN, HIGH);
}

void blink(unsigned int delayms, unsigned char times = 1) {
    for (unsigned char i=0; i<times; i++) {
        if (i>0) delay(delayms);
        ledOn();
        delay(delayms);
        ledOff();
    }
}

// -----------------------------------------------------------------------------
// Hardware
// -----------------------------------------------------------------------------

void hardwareSetup() {
    Serial.begin(SERIAL_BAUDRATE);
    Serial.println();
    Serial.println();
    SPIFFS.begin();
    pinMode(LED_PIN, OUTPUT);
    ledOff();
}

void hardwareLoop() {

    // Heartbeat
    static unsigned long last_heartbeat = 0;
    if (mqttConnected()) {
        if ((millis() - last_heartbeat > HEARTBEAT_INTERVAL) || (last_heartbeat == 0)) {
            last_heartbeat = millis();
            mqttSend((char *) MQTT_HEARTBEAT_TOPIC, (char *) "1");
            DEBUG_MSG("[BEAT] Free heap: %d\n", ESP.getFreeHeap());
            DEBUG_MSG("[NTP] Time: %s\n", (char *) NTP.getTimeDateString().c_str());
        }
    }

    if ((millis() - display_last_update) > DISPLAY_UPDATE_TIME) {
        if (displayScrolling()) {
            display_last_update += 1000;
        } else if (workingMode == 1) {
            showNextChannel();
        } else {
            clearScreen();
        }
    }

}

String getIdentifier() {
    char identifier[20];
    sprintf(identifier, "%s_%06X", DEVICE, ESP.getChipId());
    return String(identifier);
}

void welcome() {

    delay(2000);
    Serial.printf("%s %s\n", (char *) APP_NAME, (char *) APP_VERSION);
    Serial.printf("%s\n%s\n\n", (char *) APP_AUTHOR, (char *) APP_WEBSITE);
    //Serial.printf("Device: %s\n", (char *) getIdentifier().c_str());
    Serial.printf("ChipID: %06X\n", ESP.getChipId());
    Serial.printf("Last reset reason: %s\n", (char *) ESP.getResetReason().c_str());
    Serial.printf("Memory size: %d bytes\n", ESP.getFlashChipSize());
    Serial.printf("Free heap: %d bytes\n", ESP.getFreeHeap());
    FSInfo fs_info;
    if (SPIFFS.info(fs_info)) {
        Serial.printf("File system total size: %d bytes\n", fs_info.totalBytes);
        Serial.printf("            used size : %d bytes\n", fs_info.usedBytes);
        Serial.printf("            block size: %d bytes\n", fs_info.blockSize);
        Serial.printf("            page size : %d bytes\n", fs_info.pageSize);
        Serial.printf("            max files : %d\n", fs_info.maxOpenFiles);
        Serial.printf("            max length: %d\n", fs_info.maxPathLength);
    }
    Serial.println();
    Serial.println();

}

// -----------------------------------------------------------------------------
// Bootstrap methods
// -----------------------------------------------------------------------------

void setup() {
    hardwareSetup();
    welcome();
    dhtSetup();
    settingsSetup();
    workingMode = getSetting("workingMode", String(DEFAULT_WORKING_MODE)).toInt();
    displaySetup();
    showWelcome();
    otaSetup();
    wifiSetup();
    mqttSetup();
    webServerSetup();
    webSocketSetup();
    ntpSetup();
}

void loop() {
    hardwareLoop();
    dhtLoop();
    settingsLoop();
    otaLoop();
    wifiLoop();
    mqttLoop();
    displayLoop();
    webServerLoop();
    webSocketLoop();
    ntpLoop();
    delay(5);
}
