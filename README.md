# RENTALITO ESP8266

The RENTALITO is a MQTT display. It is based on the Expressif ESP8266, two 32×16 RG Dot Matrix displays by SureElectronics with a Holtek HT1632C driver, a DHT22 to measure local temperature and humidity and an IR receiver.

## Hardware

![Rentalito Web Config - Topics](/images/20160901_221601ex.jpg)

The custom PCB has the minimum required components:

* ESP12 footprint
* DHT22 footprint
* IR receiver
* Two push buttons to RESET and FLASH the board
* A 2.1 5mm jack for powering with AMS1117-3V3 to power the ESP8266
* Filtering and push up resistors and capacitors
* A 4 pin header to program it

Note: the was an error in the ESP12 footprint for version 0.9.20160609 of the board and GPIO 4 and 5 were swapped, so pins for DHT22 and IR are swapped from the schematic.

## Firmware

* **WebServer for configuration** using the great [PureCSS framework][1]
* **Flashing firmware Over-The-Air** (OTA)
* Up to **3 configurable WIFI networks**
* **MQTT support**, off course
* Up to **20 MQTT topics to listen to** (you can change this value in code)
* Visual status of the received messages via the onboard LED
* Support for a **DHT22** sensor (TODO)
* Support for an **IR** receiver (TODO)
* Command line configuration

The project uses a series of state-of-the-art libraries:

* HT1632C library by me, adapted from my own [HT1632C library for Sparkcore][5] (or Particle)
* [PubSubClient][3] by Nick O'Leary
* [ArduinoJson][4] by Benoit Blanchon
* [Embedis][7] by PatternAgents
* [PureCSS][1] by Yahoo
* And some of the libraries from the [Arduino core for ESP8266][10], by @igrr (Ivan Grokhotkov), @Links2004, @me-no-dev et a,.

## Installing

### Build the web config site

Normally when you flash an ESP8266 you only flash the firmware, like for any other microcontroller. But the ESP8266 has plenty of room and normally it is split into different partitions. One such partition is used to store web files like a normal webserver. In the "Flash your board" section below you'll know how to flash this special partition, but first we will have to build it.

The build process read the HTML files, looks for the stylesheet and script files linked there, grabs them, merges them, minifies them and compresses them. Thus, a single HTML with N linked scripts and M linked CSS files is transformed in just 3 files (index.html.gz, style.css.gz and script.js.gz). This way the ESP8266 webserver can serve them really fast. Mind the ESP8266 is just a microcontroller, the webserver has a very limited capacity to hold concurrent requests, so few and lighter files are a must.

To build these files we are using **[Gulp][11]**, a build system built in [node.js][13]. So you will need node (and [npm][14], its package manager) first. [Read the documentation][12] on how to install them.

Once you have node and npm installed, go to the 'code' folder and install all the dependencies with:

```
npm install
```

It will take a minute or two. Then you are ready to build the webserver files with:

```
gulp
```

It will create a populate a 'data' folder with all the required files.

### Build the firmware

The project is ready to be build using [PlatformIO][2].
Please refer to their web page for instructions on how to install the builder.

PlatformIO will take care of some of the library dependencies, but not all the required libraries are available in the platform library manager. Some dependencies are thus checked out as submodules in GIT. So the normal initial checkout should be:

```bash
git clone git@bitbucket.org:xoseperez/rentalito-esp8266.git
git submodule init
git submodule update
```

Once you have all the code, you can check if it's working by:

```bash
> platformio run -e wire-debug
```

If it compiles you are ready to flash the firmware.

### Flash your board

The board has a header with 3V3, GND, RX and TX pins, and two buttons for RESET and FLASH. Connect the pins to your favorite FTDI-like programmer.
Since the display can be quite power hungry, my recommendation is to plug a 2.1mm center-positive 5V power supply and do not connect the 3V3 in the programming header (only RX, TX and GND). You can use the 3V3 pin **without** the 5V supply connected (there are no protection diodes), but the display may not show up. Remember: **only 3V3 voltage** in the programming header and connect RX to your FTDI TX pin and TX to your RX pin.

To enter flash mode you have to hold the FLASH button pressed while reseting the board with the RESET button.

Wire your board and flash the firmware (with ```upload```) and the file system (with ```uploadfs```):

```bash
> platformio run --target upload -e wire-debug
> platformio run --target uploadfs -e wire-debug
```

Once you have flashed it you can flash it again over-the-air using the ```ota``` environment:

```bash
> platformio run --target upload -e ota-debug
> platformio run --target uploadfs -e ota-debug
```

When using OTA environment it defaults to the IP address of the device in SoftAP mode. If you want to flash it when connected to your home network best way is to supply the IP of the device:

```bash
> platformio run --target upload -e ota-debug --upload-port 192.168.1.151
> platformio run --target uploadfs -e ota-debug --upload-port 192.168.1.151
```

Library dependencies are automatically managed via PlatformIO Library Manager or included via submodules.

## Usage

On normal boot (i.e. button not pressed) it will execute the firmware. It configures the radio, the SPIFFS memory access, the WIFI, the WebServer and MQTT connection.

Obviously the default values for WIFI network and MQTT will probably not match your requirements. The device will start in Soft AP creating a WIFI SSID named "RENTALITO". Connect with phone, PC, laptop, whatever to that network, password is "fibonacci". Once connected browse to 192.168.4.1 and you will be presented a configuration page where you will be able to define up to 3 possible WIFI networks and the MQTT configuration parameters, and topic mappings.

It will then try to connect to the configured WIFI networks one after the other. If none of the 3 attempts succeed it will default to SoftAP mode again. Once connected it will try to connect the MQTT server.

You can configure several aspects:

* up to 3 **wifi SSID and password**
* **MQTT server, port** and optionally **user and password**
* Topics to listen to, for each topic you can:
 * **topic**: MQTT topic to listen to.
 * **name**: The title of the topic, will be show as the first row of the message or as the only row if *value* is empty.
 * **value**: The template for the value, leave empty to show a one-line message with just the *name*.
 * **filter**: If not empty, only the messages with this value will pass through.
 * **prioritary**: If set the message will be displayed right after reception, otherwise it will be queued. Messages in the queue will be displayed one after the other for 10 seconds each, until they expire.
 * **expire**: Number of seconds the message will be alive after the reception, after this time it will expire and will not be shown.

For instance, the topic /home/frontdoor/open receives a 1 when the frontdoor at home is opened. I can show a "DOOR" message with the configuration in the next picture.

![Rentalito Web Config - Topics](/images/webconfig-topics.jpg)

It will display a big one-line message (value is empty) right after receiving the topic (prioritary=YES) with a value of 1 (filter=1), and it will only be displayed once (expire=0).

You can use *{value}* in the *name* and *value* fields as a placeholder for the message payload. So "{value}ºC" will be displayed as "24ºC".

On load only the topics are displayed, click on the orange buttons to show the rest of the properties for each topic. You can add more topics (up to the defined max) clicking the green "Add" button, or delete individual ones with the red "Del" button below each topic.

Changes are not commited automatically, you have to **click the Update button** to persist them. Stored values will be cleaned on every update.

The device will publish its IP to "/device/rentalito/ip" upon reboot and will send a heartbeat message every 5 minutes. Check default.h file for configuration.

## Troubleshooting

After flashing the firmware via serial do a hard reset of the device (unplug & plug). There is an issue with the ESP.reset() method that may stuck the device after an OTA update. Check [https://github.com/esp8266/Arduino/issues/1017][6] for more info.

[1]: http://purecss.io/
[2]: http://www.platformio.org
[3]: https://github.com/knolleary/pubsubclient
[4]: https://github.com/bblanchon/ArduinoJson
[5]: https://bitbucket.org/xoseperez/sparkcore-ht1632
[6]: https://github.com/esp8266/Arduino/issues/1017
[7]: https://github.com/thingSoC/embedis
[10]: https://github.com/esp8266/Arduino
[11]: http://gulpjs.com/
[12]: https://docs.npmjs.com/getting-started/installing-node
[13]: https://nodejs.org/en/
[14]: https://www.npmjs.com/
